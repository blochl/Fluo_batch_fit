#!/usr/bin/env python2
from __future__ import print_function
from __future__ import division

"""
Make the batch fitting with fast XRF linear fitting PyMca Module.

Wiki:
    http://wikiserv.esrf.fr/id16a/index.php/Batch_processing_of_fluorescence_maps_fitting

For CLI:
Execute:
    ./FluoBatchFit2.py <CONFIG_FILE> <INDIR1> [<INDIR2> ...]
Example:
    ./FluoBatchFit2.py sample1/myfile.cfg sample1/fluotomo_angle_*

For GUI:
Execute:
    ./FluoBatchFit2.py
Then:
    Select the configure file in the first window, then in the second window
    select all the directories you want to fit.

The results will be saved by default in <INDIR1>/../.., in a directory named as
the sample directory, with the suffix "_fit". In the example above it would be
"sample1_fit".

By default the results will be in tif format (32 bit), normalized, in
subdirectories which correspond to the rotation angles (also good for 2D scans
- in which case there will be just one angle).

The default output directory can be changed by modifying the "output_dir"
variable in the "Tuning" section of this script. Setting it to 'default' will
return the default output location.
"""

from PyQt5 import Qt
import os
import sys
import numpy as np
import numpy.ma as ma
import glob
import time
import h5py
import fabio
from PyMca5.PyMcaPhysics.xrf.FastXRFLinearFit import FastXRFLinearFit
from PyMca5.PyMca import EDFStack
from PyMca5.PyMca import ArraySave

########################################
################ Tuning ################
########################################
output_dir = 'default'
concentrations = 1
weight = 0
fileRoot = "IMG"
refit = False
tif = True
manual_flux = 0.0  # Must be zero, apart from VERY special cases
### Normalization specific ###
use_diode = True
diode_factors = {33.6:7500, 17.05:2500, 17.02:2500}  # Expand if needed
########################################
########################################
########################################

def get_dirs_qt(parent=None, caption=None, directory=""):
    '''
    Provide a dialog to open many directories
    '''
    w = Qt.QFileDialog(parent, caption, directory)
    w.setFileMode(Qt.QFileDialog.DirectoryOnly)
    w.setOption(Qt.QFileDialog.ShowDirsOnly)
    w.setOption(Qt.QFileDialog.DontUseNativeDialog)

    child = w.findChild(Qt.QListView, "listView")
    if child:
        child.setSelectionMode(Qt.QAbstractItemView.MultiSelection)

    child = w.findChild(Qt.QTreeView)
    if child:
        child.setSelectionMode(Qt.QAbstractItemView.MultiSelection)

    w.exec_()
    return w.selectedFiles()


def do_fitting(confFile, dataStack, weight, refit, concentrations):
    '''
    Perform the actual fitting
    '''
    t0 = time.time()
    fastFit = FastXRFLinearFit()
    fastFit.setFitConfigurationFile(confFile)
    print("Main configuring Elapsed = % s " % (time.time() - t0))
    result = fastFit.fitMultipleSpectra(y=dataStack,
                                        weight=weight,
                                        refit=refit,
                                        concentrations=concentrations)
    print("Total Elapsed = % s " % (time.time() - t0))
    return result


def get_image_lists(fit_result):
    imageNames = fit_result['names']
    try:
        images = np.concatenate((fit_result['parameters'],
                                 fit_result['concentrations']),
                                axis=0)
    except KeyError:
        images = fit_result['parameters']

    nImages = images.shape[0]
    imageList = [None] * (nImages + fit_result['uncertainties'].shape[0])
    fileImageNames = [None] * (nImages + fit_result['uncertainties'].shape[0])
    j = 0
    for i in range(nImages):
        name = imageNames[i].replace(" ", "-")
        fileImageNames[j] = name
        imageList[j] = images[i]
        j += 1
        if not imageNames[i].startswith("C("):
            # fitted parameter
            fileImageNames[j] = "s({0})".format(name)
            imageList[j] = fit_result['uncertainties'][i]
            j += 1

    return (imageList, fileImageNames)


def normalize(imageList, normArr, realflux_init, realflux_fin, inputflux):
    imshape = imageList[0].shape
    normArr = normArr[:imshape[0], :imshape[1]]  # If the scan did not complete
    flat_normArr = normArr.flatten()
    if 0 < realflux_init < 1e15:
        realflux = realflux_init
        normpix = flat_normArr[flat_normArr.nonzero()[0][0]]  # First nonzero
    elif 0 < realflux_fin < 1e15:
        print("Initial flux is larger than 1e15 or is negative",
              "(something is wrong!).\nSwitching to the final flux")
        realflux = realflux_fin
        normpix = flat_normArr[flat_normArr.nonzero()[0][-1]]  # Last nonzero
    elif not manual_flux:
        normpix = flat_normArr[flat_normArr.nonzero()[0][0]]  # First nonzero
        print("ERROR: both initial and final flux values are either too high",
              "or negative!\n"
              "Please calculate the flux manually, and set \"manual_flux\".\n"
              "FYI, the first ring current value is: {0}".format(normpix))
        sys.exit(1)
    else:
        realflux = manual_flux
        normpix = flat_normArr[flat_normArr.nonzero()[0][0]]  # First nonzero

    norm_factor_flux = realflux / inputflux
    print("Normalization factor =", norm_factor_flux)
    normArr = normArr / normpix * norm_factor_flux
    normArr = ma.masked_where(normArr == 0, normArr, copy=False)
    normList = [ (a / normArr).data for a in imageList ]

    return normList


def write_images(imagesDir, imageList, fileImageNames, tif, fileRoot):
    fileName = os.path.join(imagesDir, fileRoot + ".edf")
    ArraySave.save2DArrayListAsEDF(imageList, fileName,
                                   labels=fileImageNames)
    fileName = os.path.join(imagesDir, fileRoot + ".csv")
    ArraySave.save2DArrayListAsASCII(imageList, fileName, csv=True,
                                     labels=fileImageNames)
    if tif:
        for i, label in enumerate(fileImageNames):
            if label.startswith("s("):
                continue
            elif label.startswith("C("):
                label_id = "_" + label[2:-1] + "_mass_fraction"
            else:
                label_id  = "_" + label

            fileName = os.path.join(imagesDir,
                                    fileRoot + label_id + ".tif")
            ArraySave.save2DArrayListAsMonochromaticTiff([imageList[i]],
                                                         fileName,
                                                         labels=[label],
                                                         dtype=np.float32)


def write_areal_density_images(imagesDir, imageList, fileImageNames, tif,
                               fileRoot, concentrations, areal_dens_ratio):
    if tif and concentrations:
        for i, label in enumerate(fileImageNames):
            if label.startswith("s("):
                continue
            elif label.startswith("C("):
                label_id = "_" + label[2:-1] + "_area_density_ngmm2"
            else:
                continue

            area_density_im = imageList[i] * areal_dens_ratio
            fileName = os.path.join(imagesDir,
                                    fileRoot + label_id + ".tif")
            ArraySave.save2DArrayListAsMonochromaticTiff([area_density_im],
                                                         fileName,
                                                         labels=[label],
                                                         dtype=np.float32)


def get_mdata_path(dir):
    dir = os.path.abspath(dir)
    dname = os.path.basename(dir)
    for f in os.listdir(dir):
        if f.endswith(dname + ".h5"):
            return os.path.join(dir, f)


def get_mdata(dir):
    mdata = get_mdata_path(dir)
    with h5py.File(mdata, 'r') as hf:
        mainkey = list(hf.keys())[0]
        srot = hf[mainkey]['sample']['positioner']['value']
        srot = srot.value.strip().split()[0]
        realflux_init = hf[mainkey]['measurement']['initial']['it'].value
        energy = hf[mainkey]['measurement']['initial']['energy'].value
        realflux_fin = hf[mainkey]['measurement']['final']['it'].value
        try:
            diode_factor = diode_factors[energy]
        except KeyError:
            print("Error! Got energy = {0} keV, ".format(energy) +
                  "but I know the diode factors only for the following:")
            print(', '.join(map(str, list(diode_factors))) + " keV")
            print("Please update the \"diode_factors\" dictionary!")
            sys.exit(1)

        return (srot, realflux_init * diode_factor * 1e12,
                realflux_fin * diode_factor * 1e12)


def get_norm_arr(dir, realflux_init, realflux_fin):
    if use_diode and realflux_init < 1e15 and realflux_fin < 1e15:
        try:
            norm = fabio.open(glob.glob(os.path.join(dir, "*it*.edf"))[0])
        except IOError:
            print("Diode file not found, using current instead.")
            norm = fabio.open(glob.glob(os.path.join(dir, "*srcur*.edf"))[0])
    else:
        norm = fabio.open(glob.glob(os.path.join(dir, "*srcur*.edf"))[0])

    return norm.data


def get_conf_dirs(args, outdir='default'):
    if len(args) == 1:
        # Create Qt context
        app = Qt.QApplication([])
        # Then do what is needed...
        confFile = Qt.QFileDialog.getOpenFileName(None,
                                                  "Choose the config file")
        confFile = confFile[0]
        if not confFile:
            print("Configuration file not selected. Exiting.")
            sys.exit(1)

        directories = get_dirs_qt(None, "Please select the directories to fit")

    elif len(args) > 2:
        confFile = os.path.abspath(args[1])
        directories = [ os.path.abspath(d) for d in args[2:] ]
    else:
        print("Usage:\nCLI: " +
              args[0] + " <CFG> <INDIR1> [<INDIR2> ...]\nGUI: " +
              args[0])
        sys.exit(1)

    if outdir == 'default':
        sampledir = os.path.dirname(directories[0])
        outdir = sampledir + '_fit'

    print("Configuration file: " + confFile)
    print("Directories to fit:")
    for d in directories:
        print("-" + d)

    print("Output directory: " + outdir)
    return (confFile, directories, outdir)


def get_from_cfg(cfg):
    with open(cfg, 'r') as f:
        for line in f:
            if line.startswith("flux"):
                fluxline = line
            elif line.startswith("Matrix"):
                matline = line

    flux = fluxline.split('=')[1].strip()
    print("Input flux: {0}".format(flux))
    matdat = matline.split('=')[1].strip().replace(" ", "").split(",")
    matrix_composition = matdat[1]
    matrix_density = float(matdat[2])
    matrix_thickness = float(matdat[3])
    print("Matrix composition: {0}".format(matrix_composition))
    print("Matrix density: {0} (g/cm**3)".format(matrix_density))
    print("Matrix thickness: {0} (cm)".format(matrix_thickness))
    areal_dens_ratio = matrix_density * matrix_thickness * 1e7  # ng/mm**2
    return float(flux), areal_dens_ratio


if __name__ == '__main__':
    confFile, directories, outdir = get_conf_dirs(sys.argv, output_dir)
    inputflux, areal_dens_ratio = get_from_cfg(confFile)
    for d in directories:
        print("Working on: " + os.path.basename(d))
        (srot, realflux_init, realflux_fin) = get_mdata(d)
        fileList = glob.glob(os.path.join(d, "*_xiaS0_0001_0000_*.edf"))
        fileList = sorted(fileList)
        normArr = get_norm_arr(d, realflux_init, realflux_fin)
        dataStack = EDFStack.EDFStack(fileList, dtype=np.float32)

        result = do_fitting(confFile, dataStack, weight, refit,
                            concentrations)
        imageList, fileImageNames = get_image_lists(result)
        imageList = normalize(imageList, normArr, realflux_init, realflux_fin,
                              inputflux)
        imagesDir = os.path.join(outdir,
                                 "srot-{:07.3f}".format(float(srot))
                                 + "_" + os.path.basename(d))
        if not os.path.isdir(imagesDir):
            os.makedirs(imagesDir)

        write_images(imagesDir, imageList, fileImageNames, tif, fileRoot)
        write_areal_density_images(imagesDir, imageList, fileImageNames, tif,
                                   fileRoot, concentrations, areal_dens_ratio)
